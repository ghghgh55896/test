# coding=utf-8


import re

from airtest.utils.apkparser.apk import APK


def install_android_app(adb_client, localpath, force_reinstall=False):
    apk_info = APK(localpath)
    package_name = apk_info.package

    if re.split(":",adb_client.serialno)[0] == '127.0.0.1':
        print("device is 模拟器")


    def _get_installed_apk_version(package):
        package_info = adb_client.shell(['dumpsys', 'package', package])
        matcher = re.search(r'versionCode=(\d+)', package_info)
        if matcher:
            return int(matcher.group(1))
        return None

    try:
        apk_version = int(apk_info.androidversion_code)
    except (RuntimeError, ValueError):
        apk_version = 0
    installed_version = _get_installed_apk_version(package_name)
    print('installed version is {}, installer version is {}. force_reinstall={}'.format(installed_version, apk_version, force_reinstall))
    if installed_version is None or apk_version > installed_version or force_reinstall:
        if installed_version is not None:
            force_reinstall = True
        if hasattr(adb_client, 'pm_install'):
            adb_client.pm_install(localpath, force_reinstall)
        else:
            adb_client.install_app(localpath, force_reinstall)
        return True
    return False


def uninstall_android_app(adb_client, package):
    if hasattr(adb_client, 'pm_uninstall'):
        adb_client.pm_uninstall(package)
    else:
        adb_client.uninstall_app(package)


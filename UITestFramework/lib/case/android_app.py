# coding=utf-8

from pocounit.case import PocoTestCase
from pocounit.addons.poco.action_tracking import ActionTracker
from pocounit.addons.poco.capturing import SiteCaptor
from poco.drivers.android.uiautomation import AndroidUiautomationPoco

from airtest.core.api import connect_device, device as current_device
from airtest.core.helper import device_platform


class AndroidAppCase(PocoTestCase):
    @classmethod
    def setUpClass(cls):
        super(AndroidAppCase, cls).setUpClass()
        if not current_device():
            connect_device('Android:///')
        dev = current_device()
        meta_info_emitter = cls.get_result_emitter('metaInfo')
        if device_platform() == 'Android':
            meta_info_emitter.snapshot_device_info(dev.serialno, dev.adb.get_device_info())
            print("dev.serialno :{}, dev.adb.get_device_info():{}".format(dev.serialno, dev.adb.get_device_info()))

        cls.poco = AndroidUiautomationPoco()


        def _register_addon(*args):
            value = ""
            if args[0] == "ActionTracker":
                value = ActionTracker(args[1])
            elif args[0] == "SiteCaptor":
                value = SiteCaptor(args[1])
            else:
                return None
            args[2](value)
            return value

        # 启用动作捕捉(action tracker)
        _register_addon("ActionTracker",cls.poco,cls.register_addon)
        cls.site_capturer = _register_addon("SiteCaptor",cls.poco,cls.register_addon)

        cls._register_addon = _register_addon


# -*- encoding=utf8 -*-
__author__ = "admin"

import time

from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow
from scripts.ProjM.public_def import check_exists
from scripts.ProjM.public_def import check_view

# poco = UnityPoco(('', 5001), UnityEditorWindow())
poco = UnityPoco()

Unity_rpc = (poco.agent.rpc)

class PowerControl:
	# power logic文件
	view_power = 'PowerView'
	btn_main_power = '#btn_power'
	txt_level = '#txt_level'
	txt_power = '#txt_power'
	txt_next_time='#txt_nexttime'
	txt_total_time='#txt_totaltime'
	recover_0='--:--:--'
	first_email='cell0'
	btn_buy_item='#btn_buyitem'
	##########################################
	email_unreceived=poco('cell0').offspring('#go_receivedIcon')
	# power test文件
	btn_main_dungeon_enter = '#btn_fight'
	btn_dungeon_enter = 'cell0'
	btn_battle = '10102'
	btn_start = '#btn_start'
	btn_mail = '#btn_mail'
	buy_item = 'buyitem'
	lack_power = '#txt_count1'
	power='item2'
	##########################################
	btn_close_power = poco('PowerView')
	btn_main_close = poco('MailView').offspring('#btn_close')
	##########################################
	power_cost_diamond = poco('buyitem').child('#txt_cost')
	btn_battle_close = poco('DungeonChapterView').offspring('#btn_close')
	btn_dungeon_close = poco('DungeonView').offspring('#btn_close')
	btn_dungeon_power = poco('DungeonLevelView').offspring('#btn_1')
	player_power_num = poco('DungeonLevelView').offspring('#txt_1')
	battle_power_num = poco('#go_start').offspring('#txt_usepower')

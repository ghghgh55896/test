# -*- coding: utf-8 -*-
__author__ = "admin"
# import sys,codecs
# sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
# import sys,os,importlib

import time

from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow
from scripts.ProjM.public_def import check_exists
from scripts.ProjM.public_def import check_view
from scripts.ProjM.power.power_control import PowerControl

# poco = UnityPoco(('', 5001), UnityEditorWindow())
poco = UnityPoco()
Unity_rpc = (poco.agent.rpc)


class PowerLogic(PowerControl):

	def __init__(self, ui):
		self.ui = ui

	def power_num(self):
		txt_power = poco(self.txt_power)
		txt = txt_power.get_text()
		x = txt.split('/')
		return x[0], x[1]

	def power_recover_0(self):
		player_power = self.power_num()[0]
		limit_power = self.power_num()[1]
		if int(player_power) < int(limit_power):
			# print("体力未达到等级上限")
			a=int(limit_power)-int(player_power)
			Unity_rpc.call("UseGM", "GMController.sendGM", "add material 2#4#{}".format(a))
			poco("titlebg").click()
		check_view(poco(self.ui), poco(self.btn_main_power))
		assert poco(self.txt_next_time).get_text() == self.recover_0
		assert poco(self.txt_total_time).get_text() == self.recover_0
		poco(self.view_power).focus([0.1, 0.1]).click()
		# Unity_rpc.call("UseGM", "GMController.sendGM", "delete material 2#4#{}".format(int(limit_power)))
		# check_view(poco(self.ui), poco(self.btn_main_power))
		# if int(player_power) >= int(limit_power):
		# 	assert poco(self.txt_next_time).get_text() == self.recover_0
		# 	assert poco(self.txt_total_time).get_text() == self.recover_0
		# 	poco(self.view_power).focus([0.1, 0.1]).click()
		# else:
		# 	print("体力未达到等级上限")

	def level_power_up(self):
		player_power = self.power_num()[0]
		a = player_power
		level_num = poco(self.txt_level).get_text()
		Unity_rpc.call("UseGM", "GMController.sendGM", "set level {}".format(int(level_num) + 1))
		# print('等级提升啦 如果体力也增加了 就不会报错')
		time.sleep(1)
		player_power = self.power_num()[0]
		assert int(a) < int(player_power)


	def template_freeze(self,freeze_content, freeze_result):
		with poco.freeze() as frozen_poco:
			# TODO:这里冻结没有提取 体力测试时候要改
			for item in frozen_poco('#go_template(Clone)').offspring(freeze_content):
				if check_exists(item):
					assert item.get_text() == freeze_result
					print(item.get_text())

	def max_power_email(self):
		if int(self.power_num()[0])!=1200:
			a=1200-int(self.power_num()[0])
			Unity_rpc.call("UseGM", "GMController.sendGM", "add material 2#4#{}".format(a))
			poco("titlebg").click()
		# if int(self.power_num()[0]) == 1200:
		Unity_rpc.call("UseGM", "GMController.sendGM", "add material 2#4#1")
		self.ui.click()
		assert check_exists(self.email_unreceived) == False
		poco(self.first_email).click()
		poco(text='领取').click()
		self.template_freeze('contentText','道具已满')
		self.btn_main_close.click()
		# else:
		# 	print('体力未达到上限')

	def max_power_shop(self):
		if int(self.power_num()[0])!=1200:
			a=1200-int(self.power_num()[0])
			Unity_rpc.call("UseGM", "GMController.sendGM", "add material 2#4#{}".format(a))
			poco("titlebg").click()
		# if int(self.power_num()[0]) == 1200:
		check_view(poco(self.view_power), poco(self.btn_main_power))
		self.ui.click()
		self.template_freeze('contentText','活性数值达到最大上限')
		self.btn_close_power.focus([0.1, 0.1]).click()
		# Unity_rpc.call("UseGM", "GMController.sendGM", "delete material 2#4#1200")
		# else:
		# 	print('体力未达到上限')

	def max_power_medicine_shop(self):
		if int(self.power_num()[0]) != 1200:
			a = 1200 - int(self.power_num()[0])
			Unity_rpc.call("UseGM", "GMController.sendGM", "add material 2#4#{}".format(a))
			poco("titlebg").click()
		# if int(self.power_num()[0]) == 1200:
		Unity_rpc.call("UseGM", "GMController.sendGM", "add material 10#20#1")
		poco("titlebg").click()
		check_view(poco(self.view_power), poco(self.btn_main_power))
		self.ui.click()
		self.template_freeze('contentText','活性数值达到最大上限')
		self.btn_close_power.focus([0.1, 0.1]).click()
		Unity_rpc.call("UseGM", "GMController.sendGM", "delete material 2#4#1200")
		# else:
		# 	print('体力未达到上限')

	def buy_lack_power(self):
		if int(self.power_num()[0]) != 1200:
			check_view(poco(self.view_power), poco(self.btn_main_power))
			assert int(self.ui.get_text()) == 0
			self.ui.click()
			self.template_freeze('活性药库存不足')
			PowerControl.btn_close_power.focus([0.1, 0.1]).click()
		else:
			print('power enough')

	def buy_lack_diamond(self):
		check_view(poco(self.view_power), poco(self.btn_main_power))
		player_diamond = poco('#txt_1').get_text()
		power_cost_diamond = self.ui.get_text().split('*')[1]
		if int(player_diamond) < int(power_cost_diamond):
			poco(self.btn_buy_item).click()
			self.template_freeze('contentText','钻石不足')

	def buy_power(self, update):
		check_view(poco(self.view_power), poco(self.btn_main_power))
		a = int(self.power_num()[0])
		self.ui.click()
		time.sleep(1)
		b = int(self.power_num()[0])
		c = b - a
		assert c == update

	def buy_limit(self):
		check_view(poco(self.view_power), poco(self.btn_main_power))
		if poco('#txt_buycount').get_text().split('/')[0]==0:
			self.ui.click()
			with poco.freeze() as frozen_poco:
				for item in frozen_poco('#go_point').offspring('content'):
					if check_exists(item) == True:
						assert item.get_text() == '本日购买次数达到上限'
						print('OK')
		else:
			print('error')

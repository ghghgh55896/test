# -*- coding: utf-8 -*-
__author__ = "admin"
# import sys,codecs
# sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
# import sys,os,importlib

import time
from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow
from scripts.ProjM.public_def import check_exists
from scripts.ProjM.public_def import check_view
from scripts.ProjM.power.power_logic import PowerLogic
from scripts.ProjM.power.power_control import PowerControl

# poco = UnityPoco(('', 5001), UnityEditorWindow())
poco = UnityPoco()
Unity_rpc = (poco.agent.rpc)



def main_enter():
	check_view(poco(PowerControl.view_power), poco(PowerControl.btn_main_power))
	PowerControl.btn_close_power.focus([0.1, 0.1]).click()


def dungeon_enter():
	Unity_rpc.call("UseGM", "GMController.sendGM", "set dungeon all")
	time.sleep(3)
	poco(PowerControl.btn_main_dungeon_enter).click()
	poco(PowerControl.btn_dungeon_enter).click()
	poco(PowerControl.btn_battle).click()
	check_view(poco(PowerControl.view_power), PowerControl.btn_dungeon_power)
	PowerControl.btn_close_power.focus([0.1, 0.1]).click()
	if int(PowerControl.player_power_num.get_text()) < int(PowerControl.battle_power_num.get_text()):
		check_view(poco(PowerControl.view_power), poco(PowerControl.btn_start))
		poco(PowerControl.view_power).focus([0.1, 0.1]).click()
	else:
		print('error')
	PowerControl.btn_battle_close.click()
	PowerControl.btn_dungeon_close.click()



def power_recover_0():
	power_recover = PowerLogic(PowerControl.view_power)
	power_recover.power_recover_0()



def level_power_up():
	power_level_power_up = PowerLogic(None)
	power_level_power_up.level_power_up()



def max_power_email():
	btn_mail = poco(PowerControl.btn_mail)
	max_power_email = PowerLogic(btn_mail)
	max_power_email.max_power_email()


def max_power_shop():
	buy_item = poco(PowerControl.buy_item)
	max_power_shop = PowerLogic(buy_item)
	max_power_shop.max_power_shop()



def max_power_medicine_shop():
	lack_power = poco(PowerControl.lack_power)
	buy_lack_power = PowerLogic(lack_power)
	buy_lack_power.max_power_medicine_shop()


def buy_lack_power():
	lack_power = poco(PowerControl.lack_power)
	buy_lack_power = PowerLogic(lack_power)
	buy_lack_power.buy_lack_power()




def buy_lack_diamond():
	power_cost_diamond = PowerControl.power_cost_diamond
	buy_lack_diamond = PowerLogic(power_cost_diamond)
	buy_lack_diamond.buy_lack_diamond()
	PowerControl.btn_close_power.focus([0.1, 0.1]).click()


def buy_power():
	Unity_rpc.call("UseGM", "GMController.sendGM", "add material 10#21#1")
	poco(text='获得物品').click()
	power = poco(PowerControl.power)
	a = PowerLogic(power)
	a.buy_power(120)
	PowerControl.btn_close_power.focus([0.1, 0.1]).click()


def buy_limit():
	buy_item = poco(PowerControl.buy_item)
	buy_limit = PowerLogic(buy_item)
	buy_limit.buy_limit()
	PowerControl.btn_close_power.focus([0.1, 0.1]).click()


def test():
	main_enter()
	dungeon_enter()
	power_recover_0()
	level_power_up()
	max_power_email()
	max_power_shop()
	max_power_medicine_shop()
	buy_lack_power()
	buy_lack_diamond()
	buy_power()
	buy_limit()


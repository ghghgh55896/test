# -*- coding: utf-8 -*-
__author__ = "admin"
import sys,codecs
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
import sys,os,importlib

from poco.drivers.unity3d import UnityPoco
poco = UnityPoco()

def check_exists(ui):
	if ui.exists():
		result=(all(int(i) <= 1 for i in ui.get_position()))
	else:
		result=False
	return result

def check_view(page_view,enter_icon):
	assert check_exists(page_view) == False
	enter_icon.click()
	assert check_exists(page_view) == True




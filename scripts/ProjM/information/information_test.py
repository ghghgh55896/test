# -*- coding: utf-8 -*-
__author__ = "admin"
# import sys,codecs
# sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
# import sys,os,importlib


from airtest.core.api import *
from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow
from airtest.core.api import *
from poco.drivers.unity3d import UnityPoco
from scripts.ProjM.information.information_logic import InformationLogic
from scripts.ProjM.public_def import check_exists
from scripts.ProjM.information.information_control import InformationControl

# poco = UnityPoco(('', 5001), UnityEditorWindow())

poco = UnityPoco()
btn_back = poco(InformationControl.btn_back)


def enter():
	entrance = InformationControl.entrance
	info_enter = InformationLogic(entrance)
	info_enter.check_player_view()


def register_time():
	enter()
	entryday = InformationControl.entryday
	info_register_time = InformationLogic(entryday)
	info_register_time.check_print()
	btn_back.click()


def progress():
	enter()
	progress = InformationControl.progress
	info_progress = InformationLogic(progress)
	info_progress.check_print()
	btn_back.click()


# print(poco('#simage_headicon').attr('SingleImage'))
def head_icon():
	# TODO:GM指令 头像太少这个用例不成功 因为没有头像更换
	# poco.agent.rpc.call("UseGM", "GMController.sendGM", "add material 1#300201#1")
	enter()
	head_icon = InformationControl.head_icon
	info_head_icon = InformationLogic(head_icon)
	info_head_icon.change_head_icon()
	btn_back.click()


def signature():
	enter()
	signature = InformationControl.signature
	input = InformationControl.input
	info_signature = InformationLogic(signature)
	info_signature.check_write_print(input)
	btn_back.click()
	btn_back.click()


def collection():
	enter()
	collection = InformationControl.collection
	info_collection = InformationLogic(collection)
	info_collection.collection()
	btn_back.click()


def txt_level():
	enter()
	txt_level = InformationControl.txt_level
	if check_exists(txt_level):
		print('OK')
	else:
		print('error')
	btn_back.click()


def slider_exp():
	enter()
	slider_exp = poco(InformationControl.slider_exp)
	if check_exists(slider_exp) == True:
		print('OK')
	else:
		print('error')
	btn_back.click()


def txt_name():
	enter()
	txt_name = InformationControl.txt_name
	if check_exists(txt_name):
		print('error')
	else:
		print('error')
	btn_back.click()


def enter_icon():
	enter_level = poco(InformationControl.enter_level)
	enter_name = poco(InformationControl.enter_name)
	enter_id = poco(InformationControl.enter_id)
	if check_exists(enter_level):
		print('OK')
	else:
		print('error')
	if check_exists(enter_name):
		print('OK')
	else:
		print('error')
	if check_exists(enter_id):
		print('OK')
	else:
		print('error')


def show_character():
	enter()
	show_characters = InformationControl.show_characters
	info_show_character = InformationLogic(show_characters)
	info_show_character.show_characters()
	btn_back.click()


def test():
	enter()
	time.sleep(3)
	btn_back.click()
	time.sleep(3)
	register_time()
	time.sleep(3)
	progress()
	time.sleep(3)
	signature()
	time.sleep(3)
	collection()
	time.sleep(3)
	txt_level()
	time.sleep(3)
	slider_exp()
	time.sleep(3)
	txt_name()
	time.sleep(3)
	enter_icon()
	time.sleep(3)
	show_character()

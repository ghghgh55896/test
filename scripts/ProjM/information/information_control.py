# -*- encoding=utf8 -*-
__author__ = "admin"

from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow
from airtest.core.api import *
from poco.drivers.unity3d import UnityPoco

# poco = UnityPoco(('', 5001), UnityEditorWindow())
poco = UnityPoco()


class InformationControl:
	# logic文件
	view_info_player_view = 'PlayerView'
	view_info_icon_tip_view = 'IconTipView'
	cell_num = 'cell{}'
	btn_change = '#btn_change'
	input_signature = '#input_signature'
	txt_input_signature = '#txt_inputsignature'
	collections_num = 'collection{}'
	txt_collections_num_txt = '#txt_progress'
	btn_close = '#btn_close'
	showcharacter_num = 'showcharacter{}'
	btn_character = 'btn_Character'
	go_choose = '#go_choose'
	# test文件
	entrance = '#btn_playerinfo'
	entryday = '#txt_entryday'
	btn_back = 'btn_back'
	progress = '#txt_episodeprogress'
	head_icon = '#simage_headicon'
	signature = '#btn_signature'
	collection = 'collection'
	slider_exp = '#slider_exp'
	enter_level = '#txt_level'
	enter_name = '#txt_name'
	enter_id = '#txt_id'
	show_characters = 'showcharacters'

	txt_level = poco('leftside').offspring('#txt_level')
	txt_name = poco('playerinfo').child('#txt_name')

	input = "askjhd"

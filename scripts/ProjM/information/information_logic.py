# -*- coding: utf-8 -*-
__author__ = "admin"
# import sys,codecs
# sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
# import sys,os,importlib


from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow

from scripts.ProjM.public_def import check_exists
from scripts.ProjM.information.information_control import InformationControl
from airtest.core.api import *
from poco.drivers.unity3d import UnityPoco

# poco = UnityPoco(('', 5001), UnityEditorWindow())
poco = UnityPoco()


class InformationLogic(InformationControl):
	def __init__(self, ui):
		self.ui = poco(ui)

	def check_print(self):
		assert check_exists(self.ui) == True
		print(self.ui.get_text())

	def check_write_print(self, a):
		assert check_exists(self.ui) == True
		self.ui.click()
		poco(self.input_signature).set_text(text=a)
		assert poco(self.txt_input_signature).get_text() == a
		print(poco(self.txt_input_signature).get_text())

	def check_player_view(self):
		assert check_exists(poco(self.view_info_player_view)) == False
		self.ui.click()
		assert check_exists(poco(self.view_info_player_view)) == True
		# print('进入成功')

	def change_head_icon(self):
		self.ui.click()
		assert check_exists(poco(self.view_info_icon_tip_view)) == True
		i = 0
		a = 0
		while check_exists(poco(self.view_info_icon_tip_view).offspring(self.cell_num.format(i))):
			a = i
			i += 1
		poco(self.cell_num.format(a)).click()
		assert check_exists(poco(self.btn_change)) == True
		poco(self.btn_change).click()

	def collection(self):
		i = 1
		if not check_exists(poco(self.collections_num.format(i)).child(self.txt_collections_num_txt)):
			self.ui.click()
		while check_exists(poco(self.collections_num.format(i)).child(self.txt_collections_num_txt)):
			i += 1
		if i - 1 == 5:
			print("OK")
		else:
			print("error")

	def choose_character(self):
		o = 0
		while poco(self.cell_num.format(o)).exists():
			if poco(self.cell_num.format(o)).offspring(self.go_choose).exists():
				o += 1
			else:
				poco(self.cell_num.format(o)).click()
				o += 1
		poco(self.btn_close).click()

	def show_characters(self):
		i = 1
		while poco(self.showcharacter_num.format(i)).exists():
			if poco(self.showcharacter_num.format(i)).offspring(self.btn_character).exists():
				i += 1
			else:
				poco(self.showcharacter_num.format(i)).click()
				self.choose_character()
				i += 1

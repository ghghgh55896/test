# -*- encoding=utf8 -*-
__author__ = "admin"

import time

from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow



# poco = UnityPoco(('', 5001), UnityEditorWindow())
poco = UnityPoco()

class ShopControl:
	# shop logic
	btn_buy = '#btn_buy'
	view_double_check_view= 'StoreGoodsView'
	get_item_toast = '获得物品'
	btn_close = '#btn_close'
	############
	item_num = poco('valuebg').offspring('text')
	get_item_num = poco('cell0').offspring('count')
	player_currency = poco('StoreView').offspring(nameMatches='#txt_\d')
	cell_price = poco('StoreGoodsView').offspring('#txt_salePrice')

	#shop test
	enterance = 'StoreView'
	enter_icon = '#btn_bank'
	name = '抽卡商店·二'
	max = '#btn_max'
	min = '#btn_min'

	first_tab=poco(text='抽卡商店')
	second_tab=poco(text='高级抽卡商店')
	cell=poco(text='悠远的振响')
	max_first_tab=poco(text='抽卡商店')
	max_second_tab=poco(text='高级抽卡商店')
	max_cell=poco(text='不腐猴爪')

	min_first_tab=poco(text='抽卡商店')
	min_second_tab=poco(text='高级抽卡商店')
	min_cell=poco(text='不腐猴爪')

	insufficient_first_tab=poco(text='抽卡商店')
	insufficient_second_tab=poco(text='高级抽卡商店')
	insufficient_cell=poco(text='无声的残响')

	template=poco('#go_template(Clone)').offspring('contentText')
	insufficient = poco('#go_point').offspring('contentText')
	soldout_toast=poco('#go_point').offspring('contentText')
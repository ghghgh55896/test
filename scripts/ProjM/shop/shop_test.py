# -*- coding: utf-8 -*-
__author__ = "admin"
# import sys,codecs
# sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
# import sys,os,importlib
from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow

from scripts.ProjM.shop.shop_logic import ShopLogic
from scripts.ProjM.public_def import check_exists
from scripts.ProjM.public_def import check_view
from scripts.ProjM.shop.shop_control import ShopControl

# poco = UnityPoco(('', 5001), UnityEditorWindow())
poco = UnityPoco()
Unity_rpc = (poco.agent.rpc)


def enter():
	check_view(poco(ShopControl.enterance), poco(ShopControl.enter_icon))
	poco(ShopControl.btn_close).click()


# 限购 免费 二次确认里面有做断言都用这个
def shop_buy():
	check_view(poco(ShopControl.enterance), poco(ShopControl.enter_icon))
	ShopControl.first_tab.focus([-0.2,0.5]).click()##指定一级tab
	ShopControl.second_tab.focus([-0.2,0.5]).click()##指定二级tab
	cell = ShopControl.cell##商品
	shop_cell = ShopLogic(cell)
	shop_cell.buy()
	poco(ShopControl.btn_close).click()


# 页签 商店 都用这个
def tab_change():
	check_view(poco(ShopControl.enterance), poco(ShopControl.enter_icon))
	name = ShopControl.name
	poco(text=name).focus([-0.2,0.5]).click()
	if check_exists(ShopControl.template):
		print("ERROR")
		print(ShopControl.template.get_text())
	else:
		print(name)
		print('OK')
	poco(ShopControl.btn_close).click()


def max_buy():
	check_view(poco(ShopControl.enterance), poco(ShopControl.enter_icon))
	ShopControl.max_first_tab.focus([-0.2,0.5]).click()##指定一级tab
	ShopControl.max_second_tab.focus([-0.2,0.5]).click()##指定二级tab
	cell = ShopControl.max_cell##商品
	cell.click()
	shop_max_buy = ShopLogic(poco(ShopControl.max))
	shop_max_buy.buy_limit()
	poco(ShopControl.btn_close).click()
	poco(ShopControl.btn_close).click()



def min_buy():
	check_view(poco(ShopControl.enterance), poco(ShopControl.enter_icon))
	ShopControl.min_first_tab.focus([-0.2,0.5]).click()##指定一级tab
	ShopControl.min_second_tab.focus([-0.2,0.5]).click()##指定二级tab
	cell = ShopControl.min_cell##商品
	cell.click()
	min = poco(ShopControl.min)
	shop_max_buy = ShopLogic(min)
	shop_max_buy.buy_limit()
	poco(ShopControl.btn_close).click()
	poco(ShopControl.btn_close).click()



def buy_insufficient():
	check_view(poco(ShopControl.enterance), poco(ShopControl.enter_icon))
	ShopControl.insufficient_first_tab.focus([-0.2,0.5]).click()##指定一级tab
	ShopControl.insufficient_second_tab.focus([-0.2,0.5]).click()##指定二级tab
	cell = ShopControl.insufficient_cell##商品
	cell.click()
	insufficient=ShopControl.insufficient
	shop_buy_insufficient=ShopLogic(insufficient)
	shop_buy_insufficient.insufficient()
	poco(ShopControl.btn_close).click()



def buy_soldout():
	check_view(poco(ShopControl.enterance), poco(ShopControl.enter_icon))
	ShopControl.max_first_tab.focus([-0.2,0.5]).click()##指定一级tab
	ShopControl.max_second_tab.focus([-0.2,0.5]).click()##指定二级tab
	soldout=poco(text='售罄')
	soldout_toast=ShopControl.soldout_toast
	soldout.click()
	check_exists(soldout_toast)
	print(soldout_toast.get_text())
	poco(ShopControl.btn_close).click()


def test():
	Unity_rpc.call("UseGM", "GMController.sendGM", "add material 2#10#500")
	poco('CommonPropView').focus([0.1,0.1]).click()
	Unity_rpc.call("UseGM", "GMController.sendGM", "add material 2#11#500")
	poco('CommonPropView').focus([0.1, 0.1]).click()
	enter()
	shop_buy()
	tab_change()
	# min_buy()
	# max_buy()
	# buy_insufficient()
	buy_soldout()
	print('shopping OK')

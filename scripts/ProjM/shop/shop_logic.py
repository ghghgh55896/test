# -*- coding: utf-8 -*-
__author__ = "admin"
# import sys,codecs
# sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
# import sys,os,importlib

import time

from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow

from scripts.ProjM.public_def import check_exists
from scripts.ProjM.shop.shop_control import ShopControl

# poco = UnityPoco(('', 5001), UnityEditorWindow())
poco = UnityPoco()


class ShopLogic(ShopControl):

	def __init__(self, ui):
		self.ui = ui

	def buy(self):
		self.ui.click()
		assert check_exists(poco(self.view_double_check_view)) == True
		poco(self.btn_buy).click()
		poco(text=self.get_item_toast).click()

	def buy_limit(self):
		assert check_exists(poco(self.view_double_check_view)) == True
		self.ui.click()
		a = self.item_num.get_text()
		poco(self.btn_buy).click()
		b = self.get_item_num.get_text()
		if int(a) == 1:
			assert len(b) == 0
		else:
			assert int(a) == int(b)

	def insufficient(self):
		if int(self.cell_price.get_text()) > int(self.player_currency.get_text()):
			poco(self.btn_buy).click()
			assert check_exists(self.ui) == True
			assert check_exists(poco(text=self.get_item_toast)) == False
			print(self.ui.get_text())
			poco(self.btn_close).click()
		else:
			rest_currency = int(self.player_currency.get_text()) - int(self.cell_price.get_text())
			poco(self.btn_buy).click()
			poco(text=self.get_item_toast).click()
			# 这里变量重新获取
			self.player_currency = poco('StoreView').offspring(nameMatches='#txt_\d')
			assert int(self.player_currency.get_text()) == rest_currency

# -*- coding: utf-8 -*-
__author__ = "admin"
# import sys,codecs
# sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
# import sys,os,importlib

import time,os
from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow


# poco = UnityPoco(('', 5001), UnityEditorWindow())

poco = UnityPoco()
class SettingControl:
	# setting_logic
	view_sgraphics_view = 'settingsgraphicsview(Clone)'
	btn_close = '#btn_close'
	btn_sound_switch = '#btn_soundswitch'
	btn_download='#btn_download'
	cell_num='cell{}'
	btn_low='#btn_low'
	btn_middle = '#btn_middle'
	btn_high='#btn_high'
	btn_setting='#btn_setting'
	txt_0='<color=#B5B5B5>0</color>'
	#setting_test
	view_settingview = 'SettingsView'
	switch = '#btn_soundswitch'
	btn_download_all = '#btn_downloadall'
	cell1='cell1'
	graphic_btn='#btn_low'
	btn_tab='cell0'

	swipe_icon = poco('music').offspring('#go_musichandle')
	sound_cell = poco('SettingsVoicePackageView').offspring('cell0')



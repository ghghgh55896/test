# -*- coding: utf-8 -*-
__author__ = "admin"
# import sys,codecs
# sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
# import sys,os,importlib

import time
import re
from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow
from scripts.ProjM.setting.setting_logic import SettingLogic
from scripts.ProjM.public_def import check_exists
from scripts.ProjM.public_def import check_view
from scripts.ProjM.setting.setting_control import SettingControl

# poco = UnityPoco(('', 5001), UnityEditorWindow())
poco = UnityPoco()
Unity_rpc = (poco.agent.rpc)


view_settingview = poco(SettingControl.view_settingview)
btn_setting = poco(SettingControl.btn_setting)
btn_close=poco(SettingControl.btn_close)

Unity_rpc.call("UseGM", "GMController.sendGM", "set level 10")



def setting_enter():
	check_view(view_settingview, btn_setting)
	btn_close.click()



def sound_switch():
	check_view(view_settingview, btn_setting)
	switch = poco(SettingControl.switch)
	sound_switch = SettingLogic(switch)
	sound_switch.sound_switch()
	btn_close.click()


def music_swipe():
	check_view(view_settingview, btn_setting)
	swipe_icon = poco('music').offspring('#go_musichandle')
	sound_swipe = SettingLogic(swipe_icon)
	sound_swipe.music_swipe()
	btn_close.click()


def voice_swipe():
	check_view(view_settingview, btn_setting)
	swipe_icon = poco('voice').offspring('#go_voicehandle')
	sound_swipe = SettingLogic(swipe_icon)
	sound_swipe.voice_swipe()
	btn_close.click()


def effect_swipe():
	check_view(view_settingview, btn_setting)
	swipe_icon = poco('effect').offspring('#go_effecthandle')
	sound_swipe = SettingLogic(swipe_icon)
	sound_swipe.effect_swipe()
	btn_close.click()


def sound_not_swipe():
	check_view(view_settingview, btn_setting)
	swipe_icon = SettingControl.swipe_icon  # 可以改其他按钮拖动
	sound_swipe = SettingLogic(swipe_icon)
	sound_swipe.sound_not_swipe()
	btn_close.click()


def download_sound():
	check_view(view_settingview, btn_setting)
	sound_cell = SettingControl.sound_cell
	download_sound = SettingLogic(sound_cell)
	download_sound.download_sound()
	btn_close.click()
	btn_close.click()


def download_all():
	check_view(view_settingview, btn_setting)
	btn_download_all = poco(SettingControl.btn_download_all)
	download_all = SettingLogic(btn_download_all)
	download_all.download_all()
	btn_close.click()
	btn_close.click()



def check_graphic():
	check_view(view_settingview, btn_setting)
	check_graphic = SettingLogic(poco(SettingControl.cell1))
	check_graphic.check_graphic()
	btn_close.click()



def save_graphic():
	check_view(view_settingview, btn_setting)
	poco(text='画质').click()
	save_graphic = SettingLogic(poco(SettingControl.graphic_btn))
	save_graphic.save_graphic()
	btn_close.click()


def choose():
	check_view(view_settingview, btn_setting)
	tab = SettingLogic(poco(SettingControl.btn_tab))
	tab.choose()
	btn_close.click()

def test():
	setting_enter()
	sound_switch()
	music_swipe()
	voice_swipe()
	effect_swipe()
	sound_not_swipe()
	# download_sound()
	# download_all()
	check_graphic()
	save_graphic()
	print("setting OKKK")

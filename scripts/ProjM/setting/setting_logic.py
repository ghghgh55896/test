# -*- coding: utf-8 -*-
__author__ = "admin"
# import sys,codecs
# sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
# import sys,os,importlib

import time

from poco.drivers.unity3d import UnityPoco

from scripts.ProjM.public_def import check_exists
from scripts.ProjM.public_def import check_view
from scripts.ProjM.setting.setting_control import SettingControl

# poco = UnityPoco(('', 5001), UnityEditorWindow())

poco = UnityPoco()

class SettingLogic(SettingControl):
	def __init__(self, ui):
		self.ui = ui

	def sound_switch(self):
		assert check_exists(self.ui) == True
		if check_exists(self.ui.child(nameMatches=".*off")):
			self.ui.click()
			assert check_exists(self.ui.child(nameMatches=".*on")) == True
		else:
			self.ui.click()
			assert check_exists(self.ui.child(nameMatches=".*off")) == True

	def open_sound(self):
		if check_exists(poco(self.btn_sound_switch).child(nameMatches=".*off")):
			poco(self.btn_sound_switch).click()
		assert check_exists(poco(self.btn_sound_switch).child(nameMatches=".*on")) == True

	def music_swipe(self):
		self.open_sound()
		a = int(poco('#txt_musicvalue').get_text())  # 背景音乐
		# print(a)
		self.ui.swipe([0.02, 0])
		b = int(poco('#txt_musicvalue').get_text())
		# print(b)
		if b == 100:
			print("maxmaxmax")
		else:
			assert b > a

	def voice_swipe(self):
		self.open_sound()
		a = int(poco('#txt_voicevalue').get_text())  # 语音
		# print(a)
		self.ui.swipe([0.02, 0])
		b = int(poco('#txt_voicevalue').get_text())
		# print(b)
		if b == 100:
			print("maxmaxmax")
		else:
			assert b > a

	def effect_swipe(self):
		self.open_sound()
		a = int(poco('#txt_effectvalue').get_text())  # 音效
		# print(a)
		self.ui.swipe([0.02, 0])
		b = int(poco('#txt_effectvalue').get_text())
		# print(b)
		if b == 100:
			print("maxmaxmax")
		else:
			assert b > a

	def sound_not_swipe(self):
		if check_exists(poco(self.btn_sound_switch).child(nameMatches=".*on")):
			poco(self.btn_sound_switch).click()
		assert check_exists(poco(self.btn_sound_switch).child(nameMatches=".*off")) == True
		assert poco('#txt_musicvalue').get_text() == self.txt_0
		assert poco('#txt_voicevalue').get_text() == self.txt_0
		assert poco('#txt_effectvalue').get_text() == self.txt_0
		self.ui.swipe([0.1, 0])
		assert poco('#txt_musicvalue').get_text() == self.txt_0
		assert poco('#txt_voicevalue').get_text() == self.txt_0
		assert poco('#txt_effectvalue').get_text() == self.txt_0

	def download_sound(self):
		poco(self.btn_download).click()
		if check_exists(self.ui.offspring(nameMatches=".*downloaded$")) == True:
			self.ui.offspring(nameMatches=".*downloaded$").click()
			assert check_exists(self.ui.offspring(nameMatches=".*download$")) == False
			assert check_exists(self.ui.offspring(nameMatches=".*downloading$")) == False
		else:
			self.ui.offspring(nameMatches=".*download$").click()
			assert check_exists(self.ui.offspring(nameMatches=".*downloading$")) == True

	def download_all(self):
		poco(self.btn_download).click()
		i = 0
		while check_exists(poco(self.cell_num.format(i)))==True:
			if check_exists(poco(self.cell_num.format(i)).offspring('#go_download')) == True:
				self.ui.click()
				poco('#btn_yes').click()
				assert check_exists(poco(self.cell_num.format(i)).offspring('#go_downloading')) == True
			else:
				i+=1
		else:
			assert check_exists(self.ui)==False

	def check_graphic(self):
		check_view(poco(self.view_sgraphics_view), self.ui)
		if check_exists(poco(nameMatches=".*recommend")) == True:
			a = poco(nameMatches="#.*recommend").get_name().split('recommend')[0]
			if check_exists(poco(nameMatches="%s.*on" % a)) == True:
				print('defult graphic ok')
			else:
				print('error')
		assert check_exists(poco(text='低')) == True
		assert check_exists(poco(text='中')) == True
		assert check_exists(poco(text='高')) == True
		assert check_exists(poco(text='推荐')) == True
		poco(self.btn_middle).click()
		poco(self.btn_low).click()
		assert check_exists(poco(self.btn_low).offspring(nameMatches=".*selected$")) == True
		poco(self.btn_middle).click()
		assert check_exists(poco(self.btn_middle).offspring(nameMatches=".*selected$")) == True
		poco(self.btn_high).click()
		assert check_exists(poco(self.btn_high).offspring(nameMatches=".*selected$")) == True

	def save_graphic(self):
		self.ui.click()
		poco(self.btn_close).click()
		poco(self.btn_setting).click()
		poco(text='画质').click()
		assert check_exists(self.ui.offspring(nameMatches=".*selected")) == True

	def choose(self):
		self.ui.click()
		assert check_exists(self.ui.offspring(nameMatches=".*on")) == True

# -*- coding: utf-8 -*-
__author__ = "admin"
# import sys,codecs
# sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
import logging
import time
from airtest.core.api import *
from poco.drivers.unity3d import UnityPoco
from scripts.ProjM.public_def import check_exists
from scripts.ProjM.sign_in.sign_control import SignControl

poco = UnityPoco()

class SignInLogic(SignControl):

	def __init__(self, ui):
		self.ui = ui

	def sign_view(self):
		if check_exists(poco(self.ui)):
			logging.debug('本账号第一次登录 显示签到界面')
			self.get_item_view.click()
			poco(self.player_info).click()
		else:
			logging.debug('无签到界面')
			assert check_exists(poco(self.ui)) == False
		self.btn_GM.click()
		self.btn_sign_out.click()
		self.btn_sign_in.click()
		time.sleep(3)
		self.btn_sign_in.click()
		assert check_exists(poco(self.ui)) == False

	def check_past_month(self):
		poco(self.btn_calendar).click()
		self.activity_sign_in_view_btn_calendar.click()
		self.btn_before_month.click()
		i = 0
		while check_exists(poco(self.ui).offspring(self.cell_num.format(i))):
			assert check_exists(poco(self.ui).offspring(self.cell_num.format(i))) == True
			i += 1
		logging.debug('i')

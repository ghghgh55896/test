# -*- coding: utf-8 -*-
__author__ = "admin"
# import sys,codecs
# sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
# import sys,os,importlib


from poco.drivers.unity3d import UnityPoco
poco = UnityPoco()

class SignControl:
	#sign_logic
	cell_num = 'cell{}'
	btn_calendar = '#btn_calendar'
	player_info = 'playerinfos'

	get_item_view = poco(text='获得物品')
	btn_sign_in = poco(text='登录')
	btn_GM = poco(text='GM')
	btn_sign_out = poco(text='登出')
	activity_sign_in_view_btn_calendar = poco('ActivitySignInView').offspring('#btn_calendar')
	btn_before_month = poco('#go_month').offspring('cell0')
	#sign_test
	sign_view = 'ActivitySignInView'
	month_detail = '#go_monthdetail'
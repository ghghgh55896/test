# -*- coding: utf-8 -*-
__author__ = "admin"


from poco.drivers.unity3d import UnityPoco
from scripts.ProjM.sign_in.sign_in_logic import SignInLogic
from scripts.ProjM.sign_in.sign_control import SignControl
import logging

poco = UnityPoco()

def check_sign():
	sign_view = SignControl.sign_view
	sign_check_view = SignInLogic(sign_view)
	sign_check_view.sign_view()



def past_month():
	month_detail = SignControl.month_detail
	sign_past_month=SignInLogic(month_detail)
	sign_past_month.check_past_month()
	poco('#btn_close').click()

def test():
	check_sign()
	logging.debug("首次登录OK")
	past_month()
	logging.debug("日历OK")
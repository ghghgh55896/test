# -*- coding: utf-8 -*-
__author__ = "admin"
import sys,codecs
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
import sys,os,importlib
import time,os,sys
sys.path.append('../')

from UITestFramework.lib.case.android_app import AndroidAppCase
from UITestFramework.lib.utils.installation import install_android_app
from airtest.core.api import start_app, stop_app, Template, exists
from airtest.core.api import device as current_device
from airtest.core.api import auto_setup
from airtest.report.report import simple_report
from poco.drivers.unity3d import UnityPoco

import logging



class do_example(AndroidAppCase):
    @classmethod
    def name(cls):
        global runtime
        return '{}-{}'.format(cls.__name__,runtime)

    @classmethod
    def setUpClass(cls):
        #设置log类型，可以自行处理；
        logging.getLogger('airtest').setLevel(logging.ERROR)
        super().setUpClass()

    def setUp(self):
        #使用simple_report，必须初始化，并且logdir=True或者有参数
        outputpath = r'.\log\{}'.format(self.name())
        if not os.path.exists(outputpath):
            os.makedirs(outputpath)
        auto_setup(__file__, logdir=outputpath, devices=["Android:///", ])
        #暂时以M项目为主，这里需要配置化
        self.package_name = 'com.shenlan.projm'
        apk_path = self.R('res/app/com.shenlan.projm.apk')

        #机器没有包体的话，check_app会抛出异常，需要处理
        try:
            if not current_device().adb.check_app(self.package_name):
                install_android_app(current_device().adb, apk_path,force_reinstall=True)
        except Exception as e:
            print(e)
            install_android_app(current_device().adb, apk_path, force_reinstall=True)

        start_app(self.package_name)
        #启动包体后，需要等待初始化
        time.sleep(10)


    def runTest(self):
        import scripts.test_case1 as case1
        case1.run_case()
        import scripts.ProjM.sign_in.sign_in_test as sign_in_case
        sign_in_case.test()
        import scripts.ProjM.information.information_test as information_case
        information_case.test()
        import scripts.ProjM.setting.setting_test as setting_case
        setting_case.test()
        import scripts.ProjM.shop.shop_test as shop_case
        shop_case.test()
        import scripts.ProjM.power.power_test as power_case
        power_case.test()



    def tearDown(self):
        #runtest执行完成后，关闭app，保存报告；
        stop_app(self.package_name)
        simple_report(__file__, logpath=True, output=r'.\log\{}\log.html'.format(self.name()))



if __name__ == '__main__':
    import pocounit
    runtime = time.time()
    pocounit.main()
